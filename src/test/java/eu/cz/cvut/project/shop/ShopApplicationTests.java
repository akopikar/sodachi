package eu.cz.cvut.project.shop;

import eu.cz.cvut.project.shop.model.Games;
import eu.cz.cvut.project.shop.model.User;
import eu.cz.cvut.project.shop.reposotory.G_rep;
import eu.cz.cvut.project.shop.reposotory.U_rep;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jmx.export.notification.UnableToSendNotificationException;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class ShopApplicationTests {

    @Test
    void contextLoads() {
    }



    @Test
    void game_db_test (  @Autowired G_rep rep1) {
        Games game = new Games("TEST");
        rep1.save(game);
        assertEquals (game.getName(),rep1.findAll().get(rep1.findAll().size() - 1).getName());
        rep1.delete(game);
    }


    @Test
    void user_db_connection_test (  @Autowired U_rep rep2, @Autowired G_rep rep1) {
        Games game = new Games("TEST");
        rep1.save(game);

        User user = new User("TEST", 111,rep1.findAll().get(rep1.findAll().size() - 1));
        rep2.save(user);

        assertEquals (user.getFull_name(),rep2.findAll().get(rep2.findAll().size() - 1).getFull_name());
        assertEquals(rep1.findAll().get(rep1.findAll().size() - 1).getName(), rep2.findAll().get(rep2.findAll().size() - 1).getTmp().getName());

        rep2.delete(user);
        rep1.delete(game);
    }


}

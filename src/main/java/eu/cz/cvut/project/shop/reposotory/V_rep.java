package eu.cz.cvut.project.shop.reposotory;

import eu.cz.cvut.project.shop.model.Viewers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface V_rep
        extends JpaRepository<Viewers, Integer> { }





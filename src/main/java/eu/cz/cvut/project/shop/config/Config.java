package eu.cz.cvut.project.shop.config;

import eu.cz.cvut.project.shop.model.Games;
import eu.cz.cvut.project.shop.model.User;
import eu.cz.cvut.project.shop.model.Viewers;
import eu.cz.cvut.project.shop.reposotory.G_rep;
import eu.cz.cvut.project.shop.reposotory.U_rep;
import eu.cz.cvut.project.shop.reposotory.V_rep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PreDestroy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Configuration
public class Config {
    @Autowired
    G_rep repository;
    @Autowired
    U_rep lol;
    @Autowired
    V_rep viewers;
    @Bean
    public CommandLineRunner data()  {
        return (args) -> {
            viewers.deleteAll();
            lol.deleteAll();
            repository.deleteAll();

            repository.save(new Games("Dark souls 1"));
            repository.save(new Games("Dark souls 2"));
            repository.save(new Games("Dark souls 3"));
            repository.save(new Games("Dota 2"));
            repository.save(new Games("Prototype 2"));
            repository.save(new Games("Prototype 1"));
            repository.save(new Games("CS:GO"));

            List<Games> games =  repository.findAll();

            lol.save(new User("Evil Arthas", 15645215, games.get(0).getPoint()));
            lol.save(new User("Kitty", 25725425, games.get(0).getPoint()));
            lol.save(new User("Bester", 2572147, games.get(1).getPoint()));
            lol.save(new User("Doner", 5235258, games.get(1).getPoint()));
            lol.save(new User("Stray", 979846513, games.get(1).getPoint()));
            lol.save(new User("Ybicano", 345676543, games.get(2).getPoint()));
            lol.save(new User("Papich", 7654356, games.get(2).getPoint()));
            lol.save(new User("Meow", 91143455, games.get(3).getPoint()));
            lol.save(new User("Doable", 54365427, games.get(4).getPoint()));
            lol.save(new User("Doule", 6342525, games.get(5).getPoint()));
            lol.save(new User("Trouble", 432454624, games.get(5).getPoint()));
            lol.save(new User("Easy", 514315253, games.get(5).getPoint()));
            lol.save(new User("Not_that", 51435325, games.get(5).getPoint()));
            lol.save(new User("SSSSS", 543254325, games.get(6).getPoint()));

            viewers.save(new Viewers(lol.findAll().get(0)));
            viewers.save(new Viewers(lol.findAll().get(0)));
            viewers.save(new Viewers(lol.findAll().get(0)));
            viewers.save(new Viewers(lol.findAll().get(1)));
            viewers.save(new Viewers(lol.findAll().get(1)));
            viewers.save(new Viewers(lol.findAll().get(2)));
            viewers.save(new Viewers(lol.findAll().get(2)));
            viewers.save(new Viewers(lol.findAll().get(3)));
            viewers.save(new Viewers(lol.findAll().get(4)));
            viewers.save(new Viewers(lol.findAll().get(4)));
            viewers.save(new Viewers(lol.findAll().get(4)));
            viewers.save(new Viewers(lol.findAll().get(4)));
            viewers.save(new Viewers(lol.findAll().get(5)));
            viewers.save(new Viewers(lol.findAll().get(5)));
            viewers.save(new Viewers(lol.findAll().get(5)));
            viewers.save(new Viewers(lol.findAll().get(6)));
            viewers.save(new Viewers(lol.findAll().get(6)));
            viewers.save(new Viewers(lol.findAll().get(6)));
            viewers.save(new Viewers(lol.findAll().get(6)));
            viewers.save(new Viewers(lol.findAll().get(6)));
            viewers.save(new Viewers(lol.findAll().get(6)));
            viewers.save(new Viewers(lol.findAll().get(7)));
            viewers.save(new Viewers(lol.findAll().get(8)));
            viewers.save(new Viewers(lol.findAll().get(8)));
            viewers.save(new Viewers(lol.findAll().get(9)));
            viewers.save(new Viewers(lol.findAll().get(10)));
            viewers.save(new Viewers(lol.findAll().get(11)));

        };
    }

}

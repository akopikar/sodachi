package eu.cz.cvut.project.shop.reposotory;

import eu.cz.cvut.project.shop.model.Games;
import eu.cz.cvut.project.shop.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface U_rep
        extends JpaRepository<User, Integer> {

}




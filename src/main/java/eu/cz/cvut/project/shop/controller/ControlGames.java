package eu.cz.cvut.project.shop.controller;

import eu.cz.cvut.project.shop.model.Games;
import eu.cz.cvut.project.shop.model.User;
import eu.cz.cvut.project.shop.reposotory.G_rep;
import eu.cz.cvut.project.shop.reposotory.U_rep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class ControlGames {

    @Autowired
    G_rep repo;
    @Autowired
    U_rep repo1;

    @RequestMapping("/search")
    public String Search(Model model) {
        List<Games> games =  repo.findAll();
        model.addAttribute("games", games);
        return "Search";
    }
}

package eu.cz.cvut.project.shop.model;


import javax.persistence.*;

@Entity
@Table(name = "viewers")
public class Viewers {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "usr_id")
    private User tmp_users;

    public Viewers(){

    }
    public Viewers(User user) {
        tmp_users = user;
    }

    public Integer get_id(){
        return  id;
    }

    public User getUser() {

        return tmp_users;
    }



}


package eu.cz.cvut.project.shop.model;


import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "usr")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String full_name;
    private Integer contact_number;

    @ManyToOne
    @JoinColumn(name = "game_id")
    private Games tmp;

    @OneToMany(mappedBy = "tmp_users")
    private List<Viewers> viewer;

    public User() {
    }

    public Games getTmp() {
        return tmp;
    }

    public List<Viewers> getV(){
        return viewer;
    }

    public void setTmp(Games tmp) {
        this.tmp = tmp;
    }

    public User(String full_name, Integer contact_number, Games tmp) {
        this.full_name = full_name;
        this.contact_number = contact_number;
        this.tmp = tmp;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public Integer getContact_number() {
        return contact_number;
    }

    public void setContact_number(Integer contact_number) {
        this.contact_number = contact_number;
    }




}

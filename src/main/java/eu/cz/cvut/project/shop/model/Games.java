
package eu.cz.cvut.project.shop.model;


import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "game")
public class Games {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @OneToMany(mappedBy = "tmp")
    private List<User> games_id;

    public Games() {
    }
    public Games getPoint(){
        return this;
    }

    public List<User> getGames_id() {
        return games_id;
    }


    public void setGames_id(List<User> users) {
        games_id = users;
    }

    public Games(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}



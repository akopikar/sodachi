package eu.cz.cvut.project.shop.controller;

import eu.cz.cvut.project.shop.model.Games;
import eu.cz.cvut.project.shop.model.User;
import eu.cz.cvut.project.shop.reposotory.G_rep;
import eu.cz.cvut.project.shop.reposotory.U_rep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class ControlUsers {

    @Autowired
    G_rep repo;
    @Autowired
    U_rep repo1;

    @RequestMapping("/search/{game}")
    public  String Search1(@PathVariable String game, Model model)
    {
        List<Games> games =  repo.findAll();

        for (int i = 0; i < games.size(); i++) {
            if(games.get(i).getName().equals(game)) {

                model.addAttribute("game",games.get(i));

                List<User> users = games.get(i).getGames_id();
                model.addAttribute("user",users);

                break;
            }
        }

        return "game";
    }
}
